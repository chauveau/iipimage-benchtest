# iipimage-benchtest

## Requirements

 * [Siege](https://github.com/JoeDog/siege) - Debian package available.
 * curl - Debian package available.
 * jq - Debian package available.

## Usage

```
$ chmod u+x doit.sh
$ ./doit.sh -c <nb clients> -t <test_duration> -u <url dataset size> -endpoint <image url>
```

Example - 10 clients during 5 minutes fetching among 500 urls.

```
$ ./doit.sh -c 10 -t 5M -u 500 -endpoint https://localhost/fcgi-bin/iipsrv.fcgi?IIIF=/img.tif/
```

Help :

```
$ ./doit.sh -h
```

## Outputs

Output files are stored is the iipimage-benchtest directory : out-[test-number].txt.
