#!/bin/bash

DIRECTORY=$(cd `dirname $0` && pwd)
URLS_FILE=$DIRECTORY/urls.txt
N_CLIENTS=100
#N_RUNS=10
N_URLS=2000
TIME=1M

MAX_WIDTH=1920
MAX_HEIGHT=1080

display_usage(){
    echo ""
    echo "   IIPserver benchtest"
    echo ""
    echo "   ./doit.sh [options] -endpoint [endpoint url]"
    echo "   -h : Displays help."
    echo "   -c : Specifies number of Siege clients / concurrent users. Default = 100."
    echo "   -t : Specifies test duration. Default = 1 minute (1M)."
    echo "   -u : Specifies number of generated URLS (urls.txt). Default = 2000."
    echo "   -endpoint : Specifies IIIF image URL to query. Required."
    echo ""
    echo "   Example : "
    echo "   ./doit.sh -c 10 -t 2M -u 500 -endpoint https://localhost/fcgi-bin/iipsrv.fcgi?IIIF=/img.tif/"
    echo ""
}

while [[ "$#" -gt 0 ]]; do
    case $1 in
        -h) display_usage
            exit 0;;
        -c) N_CLIENTS="$2"; shift ;;
        -r) N_RUNS="$2"; shift ;;
        -u) N_URLS="$2"; shift ;;
        -t) TIME="$2"; shift ;;
        -endpoint) ENDPOINT="$2"; shift ;;
         *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

if [ -z $ENDPOINT ]
then
  echo ""
  echo "[ ERROR ] endpoint is required"
  display_usage
  exit 1
fi


if [ -f  $URLS_FILE ]; then
    echo "Removing existing urls file and previous CSV output."
    rm out.csv
    rm $URLS_FILE
fi

width=$(curl -s -k $ENDPOINT'info.json'|jq .width)
height=$(curl -s -k $ENDPOINT'info.json'|jq .height)

echo "Image dimensions = "$width" * "$height

for ((i=1;i<=$N_URLS;i++));
do
  w=$(( (RANDOM %  $MAX_WIDTH) + 1 ))
  h=$(( (RANDOM %  $MAX_HEIGHT) + 1 ))
  x=$(( ( RANDOM %  ($width-$w)) ))
  y=$(( ( RANDOM %  ($height-$h)) ))
  tile_width=$(( (RANDOM % $w) + 1 ))
  url=$ENDPOINT$x','$y','$w','$h'/'$tile_width',/0/default.jpg'
  echo $url >>$URLS_FILE
done

siege -v -c$N_CLIENTS -f $URLS_FILE -t$TIME> out.csv
